# WebGL implementation of Upconv7 Waifu2x

## Description

*If you want the "naive implementation" intended for learning purposes, please see the branch "bugfixes-only"*

*Only upconv7, not the higher-quality cunet model, is supported. Use another implementation, like nihui's Vulkan/NCNN one.*

This is a WebGL implementation of the upconv7 Waifu2x neural network (see: https://github.com/nagadomi/waifu2x/ ), for use when all else fails.

It's presently slower than pretty much every native implementation out there, but not slow enough to make it completely unusable.

Requires some WebGL extensions: `EXT_float_blend` and implied extensions.

These are required because accumulation into a floating-point-buffer cannot otherwise be performed.

However, apart from this, it is WebGL 1.

## Obvious Caveats

This *only* supports the upconv7 models, and only after conversion to a simpler format.
(Experimental vgg7 support exists in the code, but after ascertaining that vgg7 was just plain worse in every way I decided not to even bother making the functionality a proper part of the application.)

There are two options for this at the moment:

+ A awful-but-working patch to NCNN ( `ncnn-modelbin-snoop.patch.with.license.header.txt` ) is supplied that converts the model as it is loaded.
+ Conversion direct from waifu2x JSON files ( `convert_w2x_model.js` ).

If you can, use nihui's https://github.com/nihui/waifu2x-ncnn-vulkan as it has and defaults to models-cunet (a much better quality model)

All `upconv_7` models from https://github.com/nagadomi/waifu2x commit `bfd7fd9060af5ce9545a0a07d4e70daeee8206a7` are supplied.

It appears `upconv_7l` models are `upconv_7` models but larger. Theoretically, this could be made to work, but if the resources are available to run it is another question.

## Credits

Most code: 20kdc

Stylesheets for holding headerbar in place in `main.html`: Saphire Lattice

Model Data:

Older commits used data from nihui's repository due to the easy to browse nature of the involved NCNN code. The model data remains originally by nagadomi.

The present version has a converter to convert directly from the nagadomi data, and that has been used.


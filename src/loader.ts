// "Use of XMLHttpRequest's responseType attribute is no longer supported in the synchronous mode in window context."
class Loader {
	private _loading: number = 0;
	private _loaded: number = 0;
	private _onLoaded: (() => void) | null;
	itemStart() {
		this._loading += 1;
	}
	itemLoaded() {
		this._loaded += 1;
		if (this._loaded == this._loading) {
			if (this._onLoaded) {
				this._onLoaded();
				this._onLoaded = null;
			}
		}
	}
	start(onLoaded: () => void) {
		if (this._loaded == this._loading) {
			onLoaded();
		} else {
			this._onLoaded = onLoaded;
		}
	}
}

const WW_LIBFRG_UNITS = 4;
const WW_LIBFRG = 
"uniform highp sampler2D tex0; uniform highp sampler2D tex1; uniform highp sampler2D tex2; uniform highp sampler2D tex3;\n" +
"uniform highp vec2 tex0Size; uniform highp vec2 tex1Size; uniform highp vec2 tex2Size; uniform highp vec2 tex3Size;\n" +
"highp vec4 lookup(highp sampler2D tex, highp vec2 size, highp vec2 at) { return texture2D(tex, (at + 0.5) / size); }\n" +
"highp vec2 here() { return floor(gl_FragCoord.xy); }\n" +
"uniform highp vec2 viewportSize;\n";

function wwAssert(thing: boolean, reason: string) {
	if (!thing)
		throw new Error(reason);
}

/**
 * Holds both the image and optionally an FBO (if it has one).
 */
class WWImage {
	ctx: WWContext;
	texture: WebGLTexture;
	fbo?: WebGLFramebuffer | null;
	w: number;
	h: number;
	constructor(ctx: WWContext, tex: WebGLTexture, w: number, h: number, fbo?: WebGLFramebuffer | null) {
		this.ctx = ctx;
		this.texture = tex;
		this.fbo = fbo || null;
		this.w = w;
		this.h = h;
	}
	dispose() {
		if (this.fbo)
			this.ctx.gl.deleteFramebuffer(this.fbo);
		this.ctx.gl.deleteTexture(this.texture);
	}
}

/**
 * Rectangle program
 */
class WWRectProgram {
	program: WebGLProgram;
	positionAttrib: number;
	viewportSizeUniform: WebGLUniformLocation | null;
	texSizeUniforms: (WebGLUniformLocation | null)[];
	constructor(program: WebGLProgram, positionAttrib: number, viewportSizeUniform: WebGLUniformLocation | null, texSizeUniforms: (WebGLUniformLocation | null)[]) {
		this.program = program;
		this.positionAttrib = positionAttrib;
		this.viewportSizeUniform = viewportSizeUniform;
		this.texSizeUniforms = texSizeUniforms;
	}
}

function wwInitTexParam(ctx: WWContext): void {
	ctx.gl.texParameteri(ctx.gl.TEXTURE_2D, ctx.gl.TEXTURE_MAG_FILTER, ctx.gl.NEAREST);
	ctx.gl.texParameteri(ctx.gl.TEXTURE_2D, ctx.gl.TEXTURE_MIN_FILTER, ctx.gl.NEAREST);
	ctx.gl.texParameteri(ctx.gl.TEXTURE_2D, ctx.gl.TEXTURE_WRAP_S, ctx.gl.CLAMP_TO_EDGE);
	ctx.gl.texParameteri(ctx.gl.TEXTURE_2D, ctx.gl.TEXTURE_WRAP_T, ctx.gl.CLAMP_TO_EDGE);
}

/**
 * This is NOT meant to be a complete abstraction.
 * Just remember:
 * Default state of any glEnable except for DITHER is false.
 * Default state of the channel masks are entirely true.
 * If you enable something, set it's state too.
 * Clear state is undefined at all times.
 */
class WWContext {
	// contexts
	canvas: HTMLCanvasElement;
	gl: WebGLRenderingContext;
	glFlt: OES_texture_float;
	// base resources
	simpleVtx: WebGLShader;
	rectBuffer: WebGLBuffer;
	// other resources
	rectPrograms: Map<object, WWRectProgram>;
	// contextual stuff
	currentViewportW: number;
	currentViewportH: number;
	// the good stuff
	constructor(canvas: HTMLCanvasElement, webglAttr?: WebGLContextAttributes) {
		this.canvas = canvas;
		this.gl = canvas.getContext("webgl", webglAttr) as WebGLRenderingContext;
		this.glFlt = this.gl.getExtension("OES_texture_float") as OES_texture_float;
		this.gl.getExtension("WEBGL_color_buffer_float");
		this.gl.getExtension("EXT_float_blend");
		this.simpleVtx = this._mkShader(this.gl.VERTEX_SHADER, "attribute vec2 position; void main() { gl_Position = vec4(position, 1.0, 1.0); }");
		this.rectBuffer = this.gl.createBuffer() as WebGLBuffer;
		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.rectBuffer);
		this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array([
			-1, -1,
			-1, 1,
			1, -1,
			1, 1
		]), this.gl.STATIC_DRAW);
		this.rectPrograms = new Map();
		this.currentViewportW = this.canvas.width;
		this.currentViewportH = this.canvas.height;
	}
	_mkShader(t: number, s: string): WebGLShader {
		const shader = this.gl.createShader(t) as WebGLShader;
		this.gl.shaderSource(shader, s);
		this.gl.compileShader(shader);
		const status = this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS);
		console.log("Compiled Shader", {
			compileStatus: status,
			code: s,
			infoLog: this.gl.getShaderInfoLog(shader)
		});
		wwAssert(status, "Unable to compile shader.");
		return shader;
	}
	newProgram(f: string): WWRectProgram {
		const b = this._mkShader(this.gl.FRAGMENT_SHADER, WW_LIBFRG + f);
		const p = this.gl.createProgram() as WebGLProgram;
		this.gl.attachShader(p, this.simpleVtx);
		this.gl.attachShader(p, b);
		this.gl.linkProgram(p);
		console.log("Linked Program", {
			linkStatus: this.gl.getProgramParameter(p, this.gl.LINK_STATUS),
			infoLog: this.gl.getProgramInfoLog(p)
		});
		this.gl.useProgram(p);
		// bind texture uniforms to corresponding units
		const texSizeUniforms: (WebGLUniformLocation | null)[] = [];
		for (let i = 0; i < WW_LIBFRG_UNITS; i++) {
			const texUniform = this.gl.getUniformLocation(p, "tex" + i);
			if (texUniform != null)
				this.gl.uniform1i(texUniform, i);
			const texSizeUniform = this.gl.getUniformLocation(p, "tex" + i + "Size");
			texSizeUniforms.push(texSizeUniform);
		}
		return new WWRectProgram(p, this.gl.getAttribLocation(p, "position"), this.gl.getUniformLocation(p, "viewportSize"), texSizeUniforms);
	}
	newRT(w: number, h: number): WWImage {
		const fmt = this.gl.RGBA;
		// texture
		const tex = this.gl.createTexture() as WebGLTexture;
		this.gl.bindTexture(this.gl.TEXTURE_2D, tex);
		this.gl.texImage2D(this.gl.TEXTURE_2D, 0, fmt, w, h, 0, fmt, this.gl.FLOAT, null);
		wwInitTexParam(this);
		// fbo
		const fbo = this.gl.createFramebuffer() as WebGLFramebuffer;
		this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, fbo);
		this.gl.bindTexture(this.gl.TEXTURE_2D, tex);
		this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.COLOR_ATTACHMENT0, this.gl.TEXTURE_2D, tex, 0);
		// done!
		return new WWImage(this, tex, w, h, fbo);
	}
	setRT(tex: WWImage | null, viewportX?: number, viewportY?: number, viewportW?: number, viewportH?: number) {
		viewportX = viewportX || 0;
		viewportY = viewportY || 0;
		if (tex) {
			if (!tex.fbo)
				throw new Error("Attempted to render to unrenderable target.");
			this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, tex.fbo);
			viewportW = viewportW || tex.w;
			viewportH = viewportH || tex.h;
		} else {
			this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
			viewportW = this.canvas.width;
			viewportH = this.canvas.height;
			this.gl.viewport(0, 0, this.canvas.width, this.canvas.height);
		}
		this.gl.viewport(viewportX, viewportY, viewportW, viewportH);
		this.currentViewportW = viewportW;
		this.currentViewportH = viewportH;
	}
	/**
	 * Open low-level rectangle session
	 */
	llrOpen(program: WWRectProgram) {
		const gl = this.gl;
		gl.useProgram(program.program);
		gl.enableVertexAttribArray(program.positionAttrib);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.rectBuffer);
		gl.vertexAttribPointer(program.positionAttrib, 2, gl.FLOAT, false, 8, 0);
	}
	/**
	 * Set viewport size in low-level session (you MUST call this before doing any draws!)
	 */
	llrUpdateViewportSize(program: WWRectProgram) {
		this.gl.uniform2f(program.viewportSizeUniform, this.currentViewportW, this.currentViewportH);
	}
	/**
	 * Set texture - note, you must ensure that you run gl.activeTexture(gl.TEXTURE0);
	 */
	llrSetTexture(program: WWRectProgram, id: number, tex: WWImage) {
		const gl = this.gl;
		gl.activeTexture(gl.TEXTURE0 + id);
		gl.bindTexture(gl.TEXTURE_2D, tex.texture);
		gl.uniform2f(program.texSizeUniforms[id], tex.w, tex.h);
	}
	/**
	 * Close low-level rectangle session
	 */
	llrClose(program: WWRectProgram) {
		this.gl.disableVertexAttribArray(program.positionAttrib);
	}
	/**
	 * A simple and ultimately probably not good function to apply a simple program.
	 */
	applyProgram(tex: WWImage | null, program: WWRectProgram, inputs: WWImage[], uniforms?: object) {
		const gl = this.gl;
		this.llrOpen(program);
		this.setRT(tex);
		this.llrUpdateViewportSize(program);
		for (var i = 0; i < inputs.length; i++)
			this.llrSetTexture(program, i, inputs[i]);
		gl.activeTexture(gl.TEXTURE0);
		if (uniforms) {
			for (const k in uniforms) {
				const val = uniforms[k];
				const ul = this.gl.getUniformLocation(program.program, k);
				if ((typeof val) == "number") {
					gl.uniform1f(ul, val);
				} else if (val.length == 2) {
					gl.uniform2f(ul, val[0], val[1]);
				} else if (val.length == 3) {
					gl.uniform3f(ul, val[0], val[1], val[2]);
				} else if (val.length == 4) {
					gl.uniform4f(ul, val[0], val[1], val[2], val[3]);
				} else {
					wwAssert(false, "unknown uniform type for " + k);
				}
			}
		}
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
		this.llrClose(program);
	}
}

function wwRegisterProgram(f: string): (ctx: WWContext) => WWRectProgram {
	const key = {};
	return (ctx) => {
		let p = ctx.rectPrograms.get(key);
		if (p)
			return p;
		p = ctx.newProgram(f);
		ctx.rectPrograms.set(key, p);
		return p;
	};
}

class NetworkHostInformation {
	tileBufferSize: number;
	tileBufferContext: number;
	tileBufferContent: number;
	outtBufferCut: number;
	constructor(tbs: number, tbc: number, tbco: number, obc: number) {
		this.tileBufferSize = tbs;
		this.tileBufferContext = tbc;
		this.tileBufferContent = tbco;
		this.outtBufferCut = obc;
	}
}

interface NetworkConfig {
	info: NetworkHostInformation;
	instance(inputStack: WWStack): NetworkInstance;
	dispose(): void;
}

class NetworkInstance {
	layers: WWLayerBase[];
	dispose(): void {
		for (let i = 0; i < this.layers.length; i++)
			this.layers[i].result.dispose();
	}
}

// upconv_7

class Upconv7Config implements NetworkConfig {
	ctx: WWContext;
	info: NetworkHostInformation;
	convs: Conv33Config[];
	dcc: Deconv256443Config;
	constructor(ctx: WWContext, loadMgr: Loader, name: string) {
		this.ctx = ctx;
		// Magic numbers, disturbing these will break things
		const TILE_BUFFER_SIZE = 128;
		const TILE_BUFFER_CONTEXT = 8;
		const TILE_BUFFER_CONTENT = TILE_BUFFER_SIZE - (TILE_BUFFER_CONTEXT * 2);
		const OUTT_BUFFER_CUT = 5;
		this.info = new NetworkHostInformation(TILE_BUFFER_SIZE, TILE_BUFFER_CONTEXT, TILE_BUFFER_CONTENT, OUTT_BUFFER_CUT);
		this.convs = [
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_0.bin", name + "/snoop_bin_1.bin", 3, 16),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_2.bin", name + "/snoop_bin_3.bin", 16, 32),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_4.bin", name + "/snoop_bin_5.bin", 32, 64),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_6.bin", name + "/snoop_bin_7.bin", 64, 128),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_8.bin", name + "/snoop_bin_9.bin", 128, 128),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_10.bin", name + "/snoop_bin_11.bin", 128, 256)
		];
		this.dcc = new Deconv256443Config(ctx, loadMgr, name + "/snoop_bin_12.bin", name + "/snoop_bin_13.bin");
	}
	instance(inputStack: WWStack): Upconv7Instance {
		return new Upconv7Instance(this.ctx, this, inputStack);
	}
	dispose() {
		for (const item of this.convs)
			item.dispose();
		this.dcc.dispose();
	}
}

class Upconv7Instance extends NetworkInstance {
	ctx: WWContext;
	layers: WWLayerBase[];
	// Do be aware that inputStack is claimed and will be disposed
	constructor(ctx: WWContext, cfg: Upconv7Config, inputStack: WWStack) {
		super();
		this.ctx = ctx;
		// Build the layer array
		this.layers = [
			new WWInputLayer(inputStack)
		];
		for (const cvc of cfg.convs) {
			this.layers.push(new Conv33V4Layer(ctx, wwLastResult(this.layers), cvc));
			this.layers.push(new LeakyReluLayer(ctx, wwLastResult(this.layers)));
		}
		this.layers.push(new Deconv256443V4(ctx, wwLastResult(this.layers), cfg.dcc));
	}
}

// vgg_7

class Vgg7Config implements NetworkConfig {
	ctx: WWContext;
	info: NetworkHostInformation;
	convs: Conv33Config[];
	fConv: Conv33Config;
	constructor(ctx: WWContext, loadMgr: Loader, name: string) {
		this.ctx = ctx;
		// Magic numbers, disturbing these will break things
		const TILE_BUFFER_SIZE = 128;
		const TILE_BUFFER_CONTEXT = 7;
		const TILE_BUFFER_CONTENT = 113;
		const OUTT_BUFFER_CUT = 7;
		this.info = new NetworkHostInformation(TILE_BUFFER_SIZE, TILE_BUFFER_CONTEXT, TILE_BUFFER_CONTENT, OUTT_BUFFER_CUT);
		this.convs = [
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_0.bin", name + "/snoop_bin_1.bin", 3, 32),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_2.bin", name + "/snoop_bin_3.bin", 32, 32),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_4.bin", name + "/snoop_bin_5.bin", 32, 64),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_6.bin", name + "/snoop_bin_7.bin", 64, 64),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_8.bin", name + "/snoop_bin_9.bin", 64, 128),
			new Conv33Config(ctx, loadMgr, name + "/snoop_bin_10.bin", name + "/snoop_bin_11.bin", 128, 128),
		];
		this.fConv = new Conv33Config(ctx, loadMgr, name + "/snoop_bin_12.bin", name + "/snoop_bin_13.bin", 128, 3);
	}
	instance(inputStack: WWStack): Upconv7Instance {
		return new Vgg7Instance(this.ctx, this, inputStack);
	}
	dispose() {
		for (const item of this.convs)
			item.dispose();
		this.fConv.dispose();
	}
}

class Vgg7Instance extends NetworkInstance {
	ctx: WWContext;
	layers: WWLayerBase[];
	// Do be aware that inputStack is claimed and will be disposed
	constructor(ctx: WWContext, cfg: Vgg7Config, inputStack: WWStack) {
		super();
		this.ctx = ctx;
		// Build the layer array
		this.layers = [
			new WWInputLayer(inputStack)
		];
		this.layers.push(new Nearest2xLayer(ctx, wwLastResult(this.layers)));
		for (const cvc of cfg.convs) {
			this.layers.push(new Conv33V4Layer(ctx, wwLastResult(this.layers), cvc));
			this.layers.push(new LeakyReluLayer(ctx, wwLastResult(this.layers)));
		}
		this.layers.push(new Conv33V4Layer(ctx, wwLastResult(this.layers), cfg.fConv));
	}
}

function appropriateConfig(ctx: WWContext, loader: Loader, modelName: string): NetworkConfig {
	if (modelName.indexOf("vgg_7") != -1)
		return new Vgg7Config(ctx, loader, modelName);
	return new Upconv7Config(ctx, loader, modelName);
}

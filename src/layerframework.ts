class WWStack {
	ctx: WWContext;
	private _images: WWImage[];
	w: number;
	h: number;
	c: number;
	constructor(ctx: WWContext, images: WWImage[], w: number, h: number) {
		this.ctx = ctx;
		this.w = w;
		this.h = h;
		this.c = images.length;
		this._images = images;
		for (const img of images) {
			wwAssert(w == img.w, "image width must match stack width");
			wwAssert(h == img.h, "image height must match stack height");
		}
	}
	slice(i: number): WWImage {
		return this._images[i];
	}
	dispose() {
		for (let i = 0; i < this._images.length; i++)
			this._images[i].dispose();
	}
}

function wwNewEmptyStack(ctx: WWContext, w: number, h: number, c: number): WWStack {
	const texs: WWImage[] = [];
	for (let channel = 0; channel < c; channel++) {
		const tex = ctx.newRT(w, h);
		texs.push(tex);
	}
	return new WWStack(ctx, texs, w, h);
}

function wwDownloadFloats(loadMgr: Loader, i: string, w: number): Float32Array {
	loadMgr.itemStart();
	const val = new Float32Array(w);
	const xhr = new XMLHttpRequest();
	xhr.responseType = "arraybuffer";
	xhr.open("GET", i, true);
	console.log("download SW: " + i + " size " + w);
	xhr.onload = () => {
		wwAssert(xhr.response != null, "XHR response null");
		const xR: ArrayBuffer = xhr.response;
		wwAssert((xR.byteLength / 4) == w, "XHR response of bad size " + (xR.byteLength / 4) + " not expected " + w);
		val.set(new Float32Array(xR));
		loadMgr.itemLoaded();
	};
	xhr.send();
	return val;
}

function wwDownloadFloatsThen(loadMgr: Loader, i: string, w: number, apply: (data: Float32Array) => void) {
	// create an item wrapping this so that we get to run the callback first
	loadMgr.itemStart();
	// run inner item in a loader to track it specifically
	const mini = new Loader();
	const array = wwDownloadFloats(mini, i, w);
	mini.start(() => {
		apply(array);
		loadMgr.itemLoaded();
	});
}

function wwNewFileStack(ctx: WWContext, loadMgr: Loader, i: string, w: number, h: number, c: number): WWStack {
	const fmt = ctx.gl.LUMINANCE;
	const texs: WWImage[] = [];
	const channelSize = w * h * 4;
	console.log("download: " + i + " size " + (w * h * c) + " (" + w + ", " + h + ", " + c + ")");
	for (let channel = 0; channel < c; channel++) {
		const tex = ctx.gl.createTexture() as WebGLTexture;
		texs.push(new WWImage(ctx, tex, w, h));
	}
	wwDownloadFloatsThen(loadMgr, i, w * h * c, (floats) => {
		for (let channel = 0; channel < c; channel++) {
			const tex = texs[channel];
			ctx.gl.bindTexture(ctx.gl.TEXTURE_2D, tex.texture);
			ctx.gl.texImage2D(ctx.gl.TEXTURE_2D, 0, fmt, w, h, 0, fmt, ctx.gl.FLOAT, new Float32Array(floats.buffer, channel * channelSize, w * h));
			wwInitTexParam(ctx);
		}
	});
	return new WWStack(ctx, texs, w, h);
}

// This converts the floats from [outTotalSubchannel][inTotalSubchannel][kernelIdx] to C:[outChannel]H:[inTotalSubchannel]W:[kernelIdx]s:[outSubchannelMod],
//  and loads the results into a set of RGBA images.
function wwNewNCNNConvToV4Stack(ctx: WWContext, loadMgr: Loader, i: string, iSubchannels: number, oSubchannels: number, kernelSizeSquared: number): WWStack {
	const texs: WWImage[] = [];
	console.log("downloadNCNNConvToV4Stack");
	const outChannels = Math.ceil(oSubchannels / 4);
	const inChannels = Math.ceil(iSubchannels / 4);
	const imgW = kernelSizeSquared;
	const imgH = inChannels * 4;
	const imgSizeF = imgW * imgH * 4;
	for (let channel = 0; channel < outChannels; channel++) {
		const tex = ctx.gl.createTexture() as WebGLTexture;
		texs.push(new WWImage(ctx, tex, imgW, imgH));
	}
	wwDownloadFloatsThen(loadMgr, i, oSubchannels * iSubchannels * kernelSizeSquared, (floats) => {
		let index = 0;
		const result = new Float32Array(outChannels * imgSizeF);
		// deposit multipliers (the 1st 4 is inSubChannel and the 2nd is outSubChannelMod)
		const kernelIdxMul = 4;
		const inTotalSubchannelMul = kernelSizeSquared * kernelIdxMul;
		wwAssert(inTotalSubchannelMul == (imgW * 4), "inconsistency W");
		const outChannelMul = inChannels * 4 * inTotalSubchannelMul;
		wwAssert(outChannelMul == imgSizeF, "inconsistency WH");
		// deposit loops
		for (let outTotalSubchannel = 0; outTotalSubchannel < oSubchannels; outTotalSubchannel++) {
			// the lower part affects s, but the upper part affects C - this is fine, just add these together
			const deposit0 = (Math.floor(outTotalSubchannel / 4) * outChannelMul) + (outTotalSubchannel % 4);
			for (let inTotalSubchannel = 0; inTotalSubchannel < iSubchannels; inTotalSubchannel++) {
				// this just affects H
				const deposit1 = deposit0 + (inTotalSubchannel * inTotalSubchannelMul);
				for (let kernelIdx = 0; kernelIdx < kernelSizeSquared; kernelIdx++) {
					// Calculate deposit location.
					// This last one affects W
					const deposit2 = deposit1 + (kernelIdx * kernelIdxMul);
					// This approach implicitly leaves unused end subchannels zero, which is always nice.
					result[deposit2] = floats[index++];
				}
			}
		}
		for (let channel = 0; channel < outChannels; channel++) {
			const tex = texs[channel];
			ctx.gl.bindTexture(ctx.gl.TEXTURE_2D, tex.texture);
			const fmt = ctx.gl.RGBA;
			ctx.gl.texImage2D(ctx.gl.TEXTURE_2D, 0, fmt, imgW, imgH, 0, fmt, ctx.gl.FLOAT, new Float32Array(result.buffer, channel * (imgSizeF * 4), imgSizeF));
			wwInitTexParam(ctx);
		}
	});
	return new WWStack(ctx, texs, imgW, imgH);
}

abstract class WWLayerBase {
	result: WWStack;
	constructor(result: WWStack) {
		this.result = result;
	}
	abstract execute(): void;
}

class WWInputLayer extends WWLayerBase {
	result: WWStack;
	constructor(result: WWStack) {
		super(result);
	}
	execute(): void {
	}
}

abstract class WWLayer extends WWLayerBase {
	ctx: WWContext;
	result: WWStack;
	constructor(ctx: WWContext, w: number, h: number, c: number) {
		super(wwNewEmptyStack(ctx, w, h, c));
		this.ctx = ctx;
	}
}

function wwLastResult(layers: WWLayerBase[]): WWStack {
	return layers[layers.length - 1].result;
}

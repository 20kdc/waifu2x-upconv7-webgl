function main() {
	// Elements
	const statusElement = document.getElementById("statusDiv") as HTMLElement;
	const fileElement = document.getElementById("imageIn") as HTMLInputElement;
	const modelNameElement = document.getElementById("modelName") as HTMLSelectElement;
	const sourceLayerElement = document.getElementById("sourceLayer") as HTMLSelectElement;
	const sourceChannelRElement = document.getElementById("sourceChannelR") as HTMLInputElement;
	const sourceChannelGElement = document.getElementById("sourceChannelG") as HTMLInputElement;
	const sourceChannelBElement = document.getElementById("sourceChannelB") as HTMLInputElement;
	const scGElement = document.getElementById("scGScaleButton") as HTMLButtonElement;
	const scRElement = document.getElementById("scRScaleButton") as HTMLButtonElement;
	const scNxtElement = document.getElementById("scNxtButton") as HTMLButtonElement;
	const rbElement = document.getElementById("runButton") as HTMLButtonElement;
	const ccElement = document.getElementById("cancelButton") as HTMLButtonElement;

	// Defaults
	statusElement.innerText = "Loading...";
	const DEFAULT_MODEL = "models/upconv_7/art/scale2.0x_model";
	modelNameElement.value = DEFAULT_MODEL;
	sourceLayerElement.value = "-1";
	sourceChannelRElement.value = "0";
	sourceChannelGElement.value = "1";
	sourceChannelBElement.value = "2";

	// Context
	const ctx = new WWContext(document.getElementById("canvas") as HTMLCanvasElement, {preserveDrawingBuffer: true});
	ctx.canvas.width = 1;
	ctx.canvas.height = 1;

	// This is used to prevent overlapping tasks.
	// Only set to true or use complete to set to false.
	let isBusy: boolean = true;
	let recommendCancel: (() => void) | null = null;

	function complete(text: string) {
		statusElement.innerText = text;
		if (recommendCancel) {
			const rc = recommendCancel;
			recommendCancel = null;
			// business continues onto next function
			rc();
		} else {
			isBusy = false;
		}
	}

	function startTask(fn: () => void) {
		if (isBusy) {
			if (recommendCancel) {
				alert("Busy!");
				return;
			} else {
				recommendCancel = fn;
			}
		} else {
			isBusy = true;
			fn();
		}
	}

	// Initial Loadables
	const initialLoader = new Loader();

	initialLoader.itemStart();
	let currentImage = new Image();
	currentImage.src = "w2wbinit.png";
	currentImage.onload = () => {
		initialLoader.itemLoaded();
	};

	let currentModel: NetworkConfig = appropriateConfig(ctx, initialLoader, DEFAULT_MODEL);

	// Settings That Are Not Current Model/Image
	let sourceLayer: number = -1;
	let sourceChannelR: number = 0;
	let sourceChannelG: number = 1;
	let sourceChannelB: number = 2;

	// The actual main thing that does the stuff
	const runActualProcess = (theImage: HTMLImageElement, done: () => void) => {
		const theTexture = ctx.gl.createTexture() as WebGLTexture;
		ctx.gl.bindTexture(ctx.gl.TEXTURE_2D, theTexture);
		ctx.gl.texImage2D(ctx.gl.TEXTURE_2D, 0, ctx.gl.RGBA, ctx.gl.RGBA, ctx.gl.UNSIGNED_BYTE, theImage);
		wwInitTexParam(ctx);
		const theTextureSlice = new WWImage(ctx, theTexture, theImage.naturalWidth, theImage.naturalHeight);

		// Clear screen.
		ctx.setRT(null);
		ctx.gl.clearColor(0.0, 0.0, 0.0, 1.0);
		ctx.gl.clear(ctx.gl.COLOR_BUFFER_BIT);

		// Firstly. The target size is twice the size of the input, exactly.
		const targetBuffer = wwNewEmptyStack(ctx, theImage.naturalWidth * 2, theImage.naturalHeight * 2, 1);
		ctx.canvas.width = targetBuffer.w;
		ctx.canvas.height = targetBuffer.h;

		// The target buffer should show a darkened original-res version of the original image.
		ctx.applyProgram(targetBuffer.slice(0), previewProgram(ctx), [theTextureSlice]);
		// Show that.
		ctx.applyProgram(null, copyVNAProgram(ctx), [targetBuffer.slice(0)]);

		// Magical numbers. Screwing with these will probably cause really tiny graphical glitches.
		// Be careful.
		const TILE_BUFFER_SIZE = currentModel.info.tileBufferSize;
		let TILE_BUFFER_CONTEXT = currentModel.info.tileBufferContext;
		let TILE_BUFFER_CONTENT = currentModel.info.tileBufferContent;
		let OUTT_BUFFER_CUT = currentModel.info.outtBufferCut;
		let OUTT_BUFFER_CONTENT = TILE_BUFFER_CONTENT * 2;
		// Secondly. This tile buffer is what's actually used for each tile. It contains a 32x32 centre input,
		//  with TILE_BUFFER_CONTEXT pixels on either side of context, to be converted into a 64x64 output.
		const tileBuffer = wwNewEmptyStack(ctx, TILE_BUFFER_SIZE, TILE_BUFFER_SIZE, 1);

		// Thirdly. This is the instance that processes the tiles.
		const instance = currentModel.instance(tileBuffer);

		// Now that's done, get this ready:
		const cleanupEverything = () => {
			// disposes tileBuffer
			instance.dispose();

			// get rid of this too
			targetBuffer.dispose();

			// get rid of original image
			ctx.gl.deleteTexture(theTexture);
		};

		// Prepare messing-around-tools
		if (sourceLayer >= 0) {
			// messing around mode is enabled - let's play some tricks to make it work
			// firstly, remove all unwanted layers
			while (instance.layers.length > sourceLayer + 1) {
				// Remove & dispose layers after the source layer
				instance.layers.pop()!.result.dispose();
			}
			// secondly, adjust output buffer constants to display all information for a given tile
			const oLayer = instance.layers[sourceLayer];
			if (!oLayer) {
				cleanupEverything();
				complete("Invalid sourceLayer");
				return;
			}
			OUTT_BUFFER_CUT = 0;
			OUTT_BUFFER_CONTENT = oLayer.result.w;
			// thirdly, we want to extract a given set of source subchannels and possibly reorder them
			// start by just doing a mass split (wasteful, I know, but this is debug)
			const spLayer = new SplitRGBALayer(ctx, oLayer.result, true);
			instance.layers.push(spLayer);
			const oSliceR = spLayer.result.slice(sourceChannelR);
			const oSliceG = spLayer.result.slice(sourceChannelG);
			const oSliceB = spLayer.result.slice(sourceChannelB);
			if ((!oSliceR) || (!oSliceG) || (!oSliceB)) {
				cleanupEverything();
				complete("Invalid sourceChannel (only " + spLayer.result.c + " here)");
				return;
			}
			instance.layers.push(new JoinRGBLayer(ctx, new WWStack(ctx, [oSliceR, oSliceG, oSliceB], spLayer.result.w, spLayer.result.h)));
		}

		const sourceSliceArr = [wwLastResult(instance.layers).slice(0)];

		// Fourth. This is the tile schedule, that decides the order of the tiles that get converted.
		const tileSchedule: number[][] = [];
		for (let y = 0; y < theImage.naturalHeight; y += TILE_BUFFER_CONTENT)
			for (let x = 0; x < theImage.naturalWidth; x += TILE_BUFFER_CONTENT)
				tileSchedule.push([x, y]);

		// Fifth. The tile function.
		const runTile = (tileInX: number, tileInY: number) => {
			// Extract.
			ctx.applyProgram(tileBuffer.slice(0), copyOffsetProgram(ctx), [theTextureSlice], {
				offset: [tileInX - TILE_BUFFER_CONTEXT, tileInY - TILE_BUFFER_CONTEXT]
			});

			// Execute.
			for (let i = 0; i < instance.layers.length; i++)
				instance.layers[i].execute();

			// Patch.
			const patchX = (tileInX / TILE_BUFFER_CONTENT) * OUTT_BUFFER_CONTENT;
			const patchY = (tileInY / TILE_BUFFER_CONTENT) * OUTT_BUFFER_CONTENT;
			ctx.gl.enable(ctx.gl.SCISSOR_TEST);
			ctx.gl.scissor(patchX, patchY, OUTT_BUFFER_CONTENT, OUTT_BUFFER_CONTENT);
			ctx.applyProgram(targetBuffer.slice(0), copyOffsetProgram(ctx), sourceSliceArr, {
				offset: [OUTT_BUFFER_CUT - patchX, OUTT_BUFFER_CUT - patchY]
			});
			ctx.gl.disable(ctx.gl.SCISSOR_TEST);

			// Update display (this intermediate exists so copyVNA can be run without interference)
			ctx.applyProgram(null, copyVNAProgram(ctx), [targetBuffer.slice(0)]);
		};

		// Sixth. The shutdown function.
		const shutdown = () => {
			cleanupEverything();
			done();
		};

		// Seventh. Run tile schedule.
		let tileScheduler: () => void;
		tileScheduler = () => {
			const nextPos = tileSchedule.shift();
			if (recommendCancel || !nextPos) {
				shutdown();
				return;
			}
			runTile(nextPos[0], nextPos[1]);
			window.requestAnimationFrame(tileScheduler);
		};
		window.requestAnimationFrame(tileScheduler);
	};

	initialLoader.start(() => {
		statusElement.innerText = "Running test scaling...";
		runActualProcess(currentImage, () => {
			complete("Ready.");
		});
	});

	// Event Handlers
	rbElement.onclick = () => {
		startTask(() => {
			statusElement.innerText = "Running...";
			runActualProcess(currentImage, () => {
				complete("Completed.");
			});
		});
	};

	ccElement.onclick = () => {
		startTask(() => {
			complete("Cancelled.");
		});
	};

	fileElement.onchange = () => {
		startTask(() => {
			const files = fileElement.files as FileList;
			if (files.length >= 1) {
				statusElement.innerText = "Loading image...";
				const fr = new FileReader();
				fr.onload = () => {
					const img = new Image();
					img.onload = () => {
						currentImage = img;
						complete("Image loaded. Select model and press Run.");
					};
					img.onerror = () => {
						alert("Error with loading image.");
						complete("Error with loading image.");
					};
					img.src = fr.result as string;
				};
				fr.onerror = () => {
					alert("Error with FileReader.");
					complete("Error with FileReader.");
				};
				fr.readAsDataURL(files[0]);
			}
		});
	};

	modelNameElement.onchange = () => {
		startTask(() => {
			statusElement.innerText = "Loading model...";
			const modelLoader = new Loader();
			currentModel.dispose();
			const name = modelNameElement.value;
			currentModel = appropriateConfig(ctx, modelLoader, name);
			modelLoader.start(() => {
				complete("Model " + name + " loaded.");
			});
		});
	};

	sourceLayerElement.onchange = () => {
		startTask(() => {
			sourceLayer = parseInt(sourceLayerElement.value);
			complete("Changed source layer.");
		});
	};

	const changeSubchannels = () => {
		startTask(() => {
			sourceChannelR = sourceChannelRElement.valueAsNumber;
			sourceChannelG = sourceChannelGElement.valueAsNumber;
			sourceChannelB = sourceChannelBElement.valueAsNumber;
			complete("Changed source channels.");
		});
	};
	sourceChannelRElement.onchange = changeSubchannels;
	sourceChannelGElement.onchange = changeSubchannels;
	sourceChannelBElement.onchange = changeSubchannels;

	scGElement.onclick = () => {
		sourceChannelBElement.value = sourceChannelGElement.value = sourceChannelRElement.value;
		changeSubchannels();
	};

	scRElement.onclick = () => {
		sourceChannelGElement.valueAsNumber = sourceChannelRElement.valueAsNumber + 1;
		sourceChannelBElement.valueAsNumber = sourceChannelGElement.valueAsNumber + 1;
		changeSubchannels();
	};

	scNxtElement.onclick = () => {
		sourceChannelR = sourceChannelRElement.valueAsNumber = sourceChannelRElement.valueAsNumber += 3;
		sourceChannelG = sourceChannelGElement.valueAsNumber = sourceChannelRElement.valueAsNumber + 1;
		sourceChannelB = sourceChannelBElement.valueAsNumber = sourceChannelGElement.valueAsNumber + 1;
		startTask(() => {
			runActualProcess(currentImage, () => {
				complete("Completed.");
			});
		});
	};
}
main();

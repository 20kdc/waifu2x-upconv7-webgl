// Copy w/ offset
const copyOffsetProgram = wwRegisterProgram(
	"uniform highp vec2 offset;\n" +
	"void main() {\n" +
	"gl_FragColor = lookup(tex0, tex0Size, here() + offset);\n" +
	"}"
);

// Copy, flip, deactivate alpha
const copyVNAProgram = wwRegisterProgram(
	"void main() {\n" +
	"highp vec2 h = here();\n" +
	"gl_FragColor = vec4(lookup(tex0, tex0Size, vec2(h.x, viewportSize.y - (h.y + 1.0))).rgb, 1.0);\n" +
	"}"
);

// Copy nearest 2x scale, darken
const previewProgram = wwRegisterProgram(
	"void main() {\n" +
	"gl_FragColor = vec4(lookup(tex0, tex0Size, floor(here() / 2.0)).rgb * 0.5, 1.0);\n" +
	"}"
);

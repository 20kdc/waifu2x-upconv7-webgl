function wwLayersGetSubChannelFrag(sc: string) {
	return wwRegisterProgram("void main() { highp float v = lookup(tex0, tex0Size, here())." + sc + "; gl_FragColor = vec4(v, v, v, 1.0); }");
}
const sRLayerProgram = wwLayersGetSubChannelFrag("r");
const sGLayerProgram = wwLayersGetSubChannelFrag("g");
const sBLayerProgram = wwLayersGetSubChannelFrag("b");
const sALayerProgram = wwLayersGetSubChannelFrag("a");

class SplitRGBALayer extends WWLayer {
	input: WWStack;
	channels: number;
	constructor(ctx: WWContext, input: WWStack, withAlpha: boolean) {
		super(ctx, input.w, input.h, withAlpha ? (input.c * 4) : (input.c * 3));
		this.input = input;
		this.channels = withAlpha ? 4 : 3;
	}
	execute(): void {
		for (let i = 0; i < this.input.c; i++) {
			const inputs = [this.input.slice(i)];
			const base = i * this.channels;
			this.ctx.applyProgram(this.result.slice(base + 0), sRLayerProgram(this.ctx), inputs);
			this.ctx.applyProgram(this.result.slice(base + 1), sGLayerProgram(this.ctx), inputs);
			this.ctx.applyProgram(this.result.slice(base + 2), sBLayerProgram(this.ctx), inputs);
			if (this.channels == 4)
				this.ctx.applyProgram(this.result.slice(base + 3), sALayerProgram(this.ctx), inputs);
		}
	}
}

// Before you continue...
// Keep in mind the structure of the conv33 weights:
// C:[outChannel]H:[inTotalSubchannel]W:[kernelIdx]s:[outSubChannelMod]
function wwConv33ProgramForSubchannel(sc: string) {
	return "" +
	"uniform highp float inTotalSubchannel;\n" +
	"void main() {\n" +
	// --
	"highp vec2 h = here() + 1.0;\n" + // added 1.0 compensates for the "border" being removed
	// --
	"highp vec4 v = vec4(0.0);\n" +
	// -
	"v += lookup(tex0, tex0Size, h + vec2(-1.0, -1.0))." + sc + " * lookup(tex1, tex1Size, vec2(0.0, inTotalSubchannel));\n" +
	"v += lookup(tex0, tex0Size, h + vec2( 0.0, -1.0))." + sc + " * lookup(tex1, tex1Size, vec2(1.0, inTotalSubchannel));\n" +
	"v += lookup(tex0, tex0Size, h + vec2( 1.0, -1.0))." + sc + " * lookup(tex1, tex1Size, vec2(2.0, inTotalSubchannel));\n" +
	// -
	"v += lookup(tex0, tex0Size, h + vec2(-1.0,  0.0))." + sc + " * lookup(tex1, tex1Size, vec2(3.0, inTotalSubchannel));\n" +
	"v += lookup(tex0, tex0Size, h + vec2( 0.0,  0.0))." + sc + " * lookup(tex1, tex1Size, vec2(4.0, inTotalSubchannel));\n" +
	"v += lookup(tex0, tex0Size, h + vec2( 1.0,  0.0))." + sc + " * lookup(tex1, tex1Size, vec2(5.0, inTotalSubchannel));\n" +
	// -
	"v += lookup(tex0, tex0Size, h + vec2(-1.0,  1.0))." + sc + " * lookup(tex1, tex1Size, vec2(6.0, inTotalSubchannel));\n" +
	"v += lookup(tex0, tex0Size, h + vec2( 0.0,  1.0))." + sc + " * lookup(tex1, tex1Size, vec2(7.0, inTotalSubchannel));\n" +
	"v += lookup(tex0, tex0Size, h + vec2( 1.0,  1.0))." + sc + " * lookup(tex1, tex1Size, vec2(8.0, inTotalSubchannel));\n" +
	// --
	"gl_FragColor = v;\n" +
	"}\n";
}
const conv33ProgramR = wwRegisterProgram(wwConv33ProgramForSubchannel("r"));
const conv33ProgramG = wwRegisterProgram(wwConv33ProgramForSubchannel("g"));
const conv33ProgramB = wwRegisterProgram(wwConv33ProgramForSubchannel("b"));
const conv33ProgramA = wwRegisterProgram(wwConv33ProgramForSubchannel("a"));

class ConvolutionConfig {
	iTotalSubchannels: number;
	oTotalSubchannels: number;
	outWeightsV4: WWStack;
	outBiases: Float32Array;
	getBias(outTotalSubchannel: number): number {
		if (outTotalSubchannel >= this.outBiases.length)
			return 0;
		return this.outBiases[outTotalSubchannel];
	}
}

class Conv33Config extends ConvolutionConfig {
	constructor(ctx: WWContext, loadMgr: Loader, weightsFile: string, biasFile: string, iChannels: number, oChannels: number) {
		super();
		this.outWeightsV4 = wwNewNCNNConvToV4Stack(ctx, loadMgr, weightsFile, iChannels, oChannels, 9);
		this.outBiases = wwDownloadFloats(loadMgr, biasFile, oChannels);
		this.iTotalSubchannels = iChannels;
		this.oTotalSubchannels = oChannels;
	}
	dispose() {
		this.outWeightsV4.dispose();
	}
}

/**
 * Convolutions and deconvolutions both use this code.
 */
class ConvV4LayerBase extends WWLayer {
	input: WWStack;
	cfg: ConvolutionConfig;
	programs: WWRectProgram[];
	constructor(ctx: WWContext, w: number, h: number, input: WWStack, cfg: ConvolutionConfig) {
		super(ctx, w, h, cfg.outWeightsV4.c);
		this.input = input;
		this.cfg = cfg;
	}
	execute(): void {
		const ctx = this.ctx;
		const c33p = this.programs;
		// Clear output channels to biases
		let outTotalSubchannel = 0;
		for (let outChannel = 0; outChannel < this.cfg.outWeightsV4.c; outChannel++) {
			const biasR = this.cfg.getBias(outTotalSubchannel++);
			const biasG = this.cfg.getBias(outTotalSubchannel++);
			const biasB = this.cfg.getBias(outTotalSubchannel++);
			const biasA = this.cfg.getBias(outTotalSubchannel++);
			const outSlice = this.result.slice(outChannel);
			ctx.setRT(outSlice);
			ctx.gl.clearColor(biasR, biasG, biasB, biasA);
			ctx.gl.clear(ctx.gl.COLOR_BUFFER_BIT);
		}
		// Enable the main blending for the big composite
		ctx.gl.enable(ctx.gl.BLEND);
		ctx.gl.blendEquation(ctx.gl.FUNC_ADD);
		ctx.gl.blendFunc(ctx.gl.ONE, ctx.gl.ONE);
		// The composite is divided into subchannels so that the same LLR session (i.e. program setup) is reused as long as possible.
		for (let inSubchannel = 0; inSubchannel < 4; inSubchannel++) {
			const program = c33p[inSubchannel];
			ctx.llrOpen(program);
			const inTotalSubchannelUniform = ctx.gl.getUniformLocation(program.program, "inTotalSubchannel");
			for (let outChannel = 0; outChannel < this.cfg.outWeightsV4.c; outChannel++) {
				ctx.setRT(this.result.slice(outChannel));
				// All textures are of the same size.
				if (outChannel == 0)
					ctx.llrUpdateViewportSize(program);
				// Output weight slice is per-output-channel, so set it here.
				ctx.llrSetTexture(program, 1, this.cfg.outWeightsV4.slice(outChannel));
				// Finally, iterate over input channels, applying the current subchannel.
				for (let inChannel = 0; (inChannel * 4) < this.cfg.iTotalSubchannels; inChannel++) {
					const inTotalSubchannel = (inChannel * 4) + inSubchannel;
					if (inTotalSubchannel >= this.cfg.iTotalSubchannels)
						break;
					ctx.llrSetTexture(program, 0, this.input.slice(inChannel));
					ctx.gl.uniform1f(inTotalSubchannelUniform, inTotalSubchannel);
					ctx.gl.drawArrays(ctx.gl.TRIANGLE_STRIP, 0, 4);
				}
			}
			ctx.llrClose(c33p[inSubchannel]);
		}
		ctx.gl.disable(ctx.gl.BLEND);
	}
}

/**
 * This layer runs convolutions in vec4 groups.
 */
class Conv33V4Layer extends ConvV4LayerBase {
	constructor(ctx: WWContext, input: WWStack, cfg: Conv33Config) {
		super(ctx, input.w - 2, input.h - 2, input, cfg);
		this.input = input;
		this.cfg = cfg;
		this.programs = [
			conv33ProgramR(ctx),
			conv33ProgramG(ctx),
			conv33ProgramB(ctx),
			conv33ProgramA(ctx)
		];
	}
}

const reluLayerProgram = wwRegisterProgram(
	"void main() {\n" +
	"highp vec4 v = lookup(tex0, tex0Size, here());\n" +
	"gl_FragColor = max(v, 0.0) + (min(v, 0.0) * 0.1);\n" +
	"}\n"
);

class LeakyReluLayer extends WWLayer {
	input: WWStack;
	constructor(ctx: WWContext, input: WWStack) {
		super(ctx, input.w, input.h, input.c);
		this.input = input;
	}
	execute() {
		const program = reluLayerProgram(this.ctx);
		this.ctx.llrOpen(program);
		for (var i = 0; i < this.result.c; i++) {
			this.ctx.setRT(this.result.slice(i));
			// All textures are of the same size.
			if (i == 0)
				this.ctx.llrUpdateViewportSize(program);
			this.ctx.llrSetTexture(program, 0, this.input.slice(i));
			this.ctx.gl.drawArrays(this.ctx.gl.TRIANGLE_STRIP, 0, 4);
		}
		this.ctx.llrClose(program);
	}
}

function wwDeconv443ProgramForSubchannel(sc: string) {
	return "" +
	"uniform highp float inTotalSubchannel;\n" +
	"void main() {\n" +
	"highp vec2 h = here();\n" +
	"highp vec2 step = floor(h / 2.0);\n" +
	"highp vec2 subStep = h - (step * 2.0);\n" +
	"highp float subStepN = subStep.x + (subStep.y * 4.0);\n" +
	"highp vec3 v = vec3(0.0);\n" +
	"v += lookup(tex0, tex0Size, step + vec2(-1.0, -1.0))." + sc + " * lookup(tex1, tex1Size, vec2(10.0 + subStepN, inTotalSubchannel)).rgb;\n" +
	"v += lookup(tex0, tex0Size, step + vec2( 0.0, -1.0))." + sc + " * lookup(tex1, tex1Size, vec2( 8.0 + subStepN, inTotalSubchannel)).rgb;\n" +
	"v += lookup(tex0, tex0Size, step + vec2(-1.0,  0.0))." + sc + " * lookup(tex1, tex1Size, vec2( 2.0 + subStepN, inTotalSubchannel)).rgb;\n" +
	"v += lookup(tex0, tex0Size, step + vec2( 0.0,  0.0))." + sc + " * lookup(tex1, tex1Size, vec2( 0.0 + subStepN, inTotalSubchannel)).rgb;\n" +
	"gl_FragColor = vec4(v, 1.0);\n" +
	"}\n";
}

const deconv443ProgramR = wwRegisterProgram(wwDeconv443ProgramForSubchannel("r"));
const deconv443ProgramG = wwRegisterProgram(wwDeconv443ProgramForSubchannel("g"));
const deconv443ProgramB = wwRegisterProgram(wwDeconv443ProgramForSubchannel("b"));
const deconv443ProgramA = wwRegisterProgram(wwDeconv443ProgramForSubchannel("a"));

class Deconv256443Config extends ConvolutionConfig {
	constructor(ctx: WWContext, loadMgr: Loader, weightsFile: string, biasFile: string) {
		super();
		// 4x4 kernel, 256 input channels, 3 output channels
		this.outWeightsV4 = wwNewNCNNConvToV4Stack(ctx, loadMgr, weightsFile, 256, 3, 16);
		this.outBiases = wwDownloadFloats(loadMgr, biasFile, 3);
		this.iTotalSubchannels = 256;
		this.oTotalSubchannels = 3;
	}
	dispose() {
		this.outWeightsV4.dispose();
	}
}

class Deconv256443V4 extends ConvV4LayerBase {
	outBiases: Float32Array;
	constructor(ctx: WWContext, input: WWStack, cfg: Deconv256443Config) {
		super(ctx, (input.w * 2) + 2, (input.h * 2) + 2, input, cfg);
		this.input = input;
		this.programs = [
			deconv443ProgramR(ctx),
			deconv443ProgramG(ctx),
			deconv443ProgramB(ctx),
			deconv443ProgramA(ctx)
		];
	}
}

const joinRGBLayerProgram = wwRegisterProgram("void main() { highp vec2 h = here(); gl_FragColor = vec4(lookup(tex0, tex0Size, h).r, lookup(tex1, tex1Size, h).g, lookup(tex2, tex2Size, h).b, 1.0); }");

class JoinRGBLayer extends WWLayer {
	input: WWStack;
	constructor(ctx: WWContext, input: WWStack) {
		super(ctx, input.w, input.h, 1);
		wwAssert(input.c >= 3, "JoinRGB requires 3 channels");
		this.input = input;
	}
	execute(): void {
		this.ctx.applyProgram(this.result.slice(0), joinRGBLayerProgram(this.ctx), [this.input.slice(0), this.input.slice(1), this.input.slice(2)]);
	}
}

// This program needs to emulate bilinear filtering. Yes, it has to be emulated.
const bilinear2xLayerProgram = wwRegisterProgram(
	"void main() {\n" +
	"highp vec2 h2 = here() / 2.0;\n" +
	"highp vec2 a = floor(h2);\n" +
	"highp vec2 aP = h2 - a;\n" +
	"highp vec2 b = a + vec2(1.0, 0.0);\n" +
	"highp vec2 c = a + vec2(0.0, 1.0);\n" +
	"highp vec2 d = a + vec2(1.0, 1.0);\n" +
	"highp vec4 ac = lookup(tex0, tex0Size, a);\n" +
	"highp vec4 bc = lookup(tex0, tex0Size, b);\n" +
	"highp vec4 cc = lookup(tex0, tex0Size, c);\n" +
	"highp vec4 dc = lookup(tex0, tex0Size, d);\n" +
	"gl_FragColor = mix(mix(ac, bc, aP.x), mix(cc, dc, aP.x), aP.y);\n" +
	"}\n"
);

class Bilinear2xLayer extends WWLayer {
	input: WWStack;
	constructor(ctx: WWContext, input: WWStack) {
		super(ctx, input.w * 2, input.h * 2, 1);
		wwAssert(input.c == 1, "Bilinear2xLayer requires 1 channel");
		this.input = input;
	}
	execute(): void {
		this.ctx.applyProgram(this.result.slice(0), bilinear2xLayerProgram(this.ctx), [this.input.slice(0)]);
	}
}

const nearest2xLayerProgram = wwRegisterProgram(
	"void main() {\n" +
	"highp vec2 h2 = here() / 2.0;\n" +
	"gl_FragColor = lookup(tex0, tex0Size, h2);\n" +
	"}\n"
);

class Nearest2xLayer extends WWLayer {
	input: WWStack;
	constructor(ctx: WWContext, input: WWStack) {
		super(ctx, input.w * 2, input.h * 2, 1);
		wwAssert(input.c == 1, "Nearest2xLayer requires 1 channel");
		this.input = input;
	}
	execute(): void {
		this.ctx.applyProgram(this.result.slice(0), nearest2xLayerProgram(this.ctx), [this.input.slice(0)]);
	}
}


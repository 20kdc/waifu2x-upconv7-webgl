/*
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/art/scale2.0x_model.json models/upconv_7/art/scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/art/noise0_scale2.0x_model.json models/upconv_7/art/noise0_scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/art/noise1_scale2.0x_model.json models/upconv_7/art/noise1_scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/art/noise2_scale2.0x_model.json models/upconv_7/art/noise2_scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/art/noise3_scale2.0x_model.json models/upconv_7/art/noise3_scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/photo/scale2.0x_model.json models/upconv_7/photo/scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/photo/noise0_scale2.0x_model.json models/upconv_7/photo/noise0_scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/photo/noise1_scale2.0x_model.json models/upconv_7/photo/noise1_scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/photo/noise2_scale2.0x_model.json models/upconv_7/photo/noise2_scale2.0x_model upconv_7
node convert_w2x_model.js ~/Documents/External/waifu2x/models/upconv_7/photo/noise3_scale2.0x_model.json models/upconv_7/photo/noise3_scale2.0x_model upconv_7

node convert_w2x_model.js ~/Documents/External/waifu2x/models/vgg_7/art/scale2.0x_model.json models/vgg_7/art/scale2.0x_model vgg_7
*/

// Converts from whatever format the waifu2x JSON models use to the format used here (a directory of flat binary files).
// A description of the format:
//
// The JSON files are arrays, one object per layer.
//
// For the convolution layers, the objects have two actually important parts:
// weight: [outChannel][inChannel][kernelX][kernelY] -> number
// bias: [outChannel] -> number
// The main thing to keep in mind is that the weight ordering is not exactly the same as NCNN uses, as the weight X and Y axis is swapped.
//
// The deconvolution layer, however, uses a different weight ordering:
// weight: [inChannel][outChannel][kernelX][kernelY] -> number

const fs = require("fs");
const process = require("process");
const inPath = process.argv[2];
const outPathPre = process.argv[3];
const type = process.argv[4];

let vgg = false;

if (type == "vgg_7") {
	vgg = true;
} else if (type == "upconv_7") {
	vgg = false;
} else {
	throw new Exception("Invalid type of network");
}

fs.mkdirSync(outPathPre, {
	recursive: true
});
const outPath = outPathPre + "/";

const jsonObj = JSON.parse(fs.readFileSync(inPath));

let blobID = 0;

function dumpBlob(array) {
	fs.writeFileSync(outPath + "snoop_bin_" + (blobID++) + ".bin", array);
}

function swapKernelAxis(index, kernel, weightFA) {
	const kernelW = kernel.length;
	const kernelH = kernel[0].length;
	for (let y = 0; y < kernelH; y++)
		for (let x = 0; x < kernelW; x++)
			weightFA[index++] = kernel[x][y];
	return index;
}

for (let layerIdx = 0; layerIdx < jsonObj.length; layerIdx++) {
	const layer = jsonObj[layerIdx];
	console.log("layer weight structure " + layer.weight.length  + " " + layer.weight[0].length + " " + layer.weight[0][0].length + " " + layer.weight[0][0][0].length);
	let weightFA = new Float32Array(layer.weight.length * layer.weight[0].length * layer.weight[0][0].length * layer.weight[0][0][0].length);
	if ((layerIdx != jsonObj.length - 1) || vgg) {
		// Convolution
		let idx = 0;
		for (let outChannel of layer.weight) {
			for (let kernel of outChannel) {
				idx = swapKernelAxis(idx, kernel, weightFA);
			}
		}
	} else {
		// Deconvolution - need to swap ordering of outChannel and inChannel
		let idx = 0;
		for (let outChannel = 0; outChannel < layer.weight[0].length; outChannel++) {
			for (let inChannel = 0; inChannel < layer.weight.length; inChannel++) {
				idx = swapKernelAxis(idx, layer.weight[inChannel][outChannel], weightFA);
			}
		}
	}
	dumpBlob(weightFA);
	// well this is trivial
	dumpBlob(new Float32Array(layer.bias));
}

Licensing is per-file:

ncnn-modelbin-snoop.patch.with.license.header.txt is a patch to NCNN for extracting ModelBin data - the necessary license header is supplied with it.

The models in `models` are converted from https://github.com/nagadomi/waifu2x - see: COPYING-waifu2x.txt

The rest (my code in src, the convert_w2x_model.js and interpolate_models.js files, the model_format_reference.txt file, the html file, etc.) is under the CC0 1.0 Universal license - see: COPYING-CC0.txt


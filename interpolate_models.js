// Just for fun.
// Results indicate that interpolating models is silly.
// node interpolate_models.js models/upconv_7/art/scale2.0x_model models/upconv_7/photo/scale2.0x_model 0.5 models/scratch

const fs = require("fs");
const process = require("process");
const inPathA = process.argv[2] + "/";
const inPathB = process.argv[3] + "/";
const amount = parseFloat(process.argv[4]);
const outPathPre = process.argv[5];
fs.mkdirSync(outPathPre, {
	recursive: true
});
const outPath = outPathPre + "/";

function readBlob(base, blobID) {
	const res = fs.readFileSync(base + "snoop_bin_" + blobID + ".bin", {});
	return new Float32Array(res.buffer, res.byteOffset, Math.floor(res.length / 4));
}
function dumpBlob(blobID, array) {
	fs.writeFileSync(outPath + "snoop_bin_" + blobID + ".bin", array);
}

for (let i = 0; i < 14; i++) {
	const blobA = readBlob(inPathA, i);
	const blobB = readBlob(inPathB, i);
	console.log("blob " + i + " : " + blobA.length + " " + blobB.length);
	const blobC = new Float32Array(blobA.length);
	for (let j = 0; j < blobC.length; j++) {
		blobC[j] = (blobA[j] * (1 - amount)) + (blobB[j] * amount)
	}
	dumpBlob(i, blobC);
}

# Waifu2x For Humans

NOTE: All information here uses constants that are specific to waifu2x.

If you think this is oversimplifying, then try to read NCNN code and tell me that if your goal was to run waifu2x, you would willingly implement all that.

# Layers

Input is theoretically 156x156 3-channel (RGB, values are from 0 to 1) but can effectively be of any size that isn't too small to run.

There are 6 convolution layers, and 1 deconvolution layer.

The individual convolution layers only differ in the number of output channels they have.
The deconvolution layer is special.

A detailed breakdown of the size and structure of the whole network is here.

```
Input                     : Theoretically 156x156x3ch, in practice any not-too-small 3ch
3x3 RELU Convolution Layer: 3ch   -> 16ch  (16 x3  x3x3=432 weights,    16  biases) - Output theoretically 154x154
3x3 RELU Convolution Layer: 16ch  -> 32ch  (32 x16 x3x3=4608 weights,   32  biases) - Output theoretically 152x152
3x3 RELU Convolution Layer: 32ch  -> 64ch  (64 x32 x3x3=18432 weights,  64  biases) - Output theoretically 150x150
3x3 RELU Convolution Layer: 64ch  -> 128ch (128x64 x3x3=73728 weights,  128 biases) - Output theoretically 148x148 
3x3 RELU Convolution Layer: 128ch -> 128ch (128x128x3x3=147456 weights, 128 biases) - Output theoretically 146x146
3x3 RELU Convolution Layer: 128ch -> 256ch (256x128x3x3=294912 weights, 256 biases) - Output theoretically 144x144
2x 4x4 Deconvolution Layer: 256ch -> 3ch   (3  x256x4x4=12288 weights, 3 biases)    - Output theoretically 290x290 (284x284 w/ 'padding' removed)
```

# 3x3 RELU Convolution Layers

The individual convolution layers only differ in the number of output channels they have.
However, this also implies differing numbers of input channels from the previous layer.

For a given output point, it reads 3x3 *relatively positioned* (i.e. 1,1 is the input that corresponds to the output) input points.

It then multiplies those input points by weights, and sums them, along with a per-output-channel bias.

The weights are arranged as follows (earlier indices are more 'major'):

`[outputChannels][inputChannels][kernelH][kernelW]`

The biases meanwhile are just a simple output channel array.

Finally, if the final value is > 0, it is multiplied by 0.1 (ncnn comments suggest this is "leakyrelu" with "slope" 0.1).
(Note that this step can be implemented as a separate layer - for simplicity, or if you'd have to do that anyway.)

However, it does not attempt to calculate output points that would cause any input point to be out of bounds.

This implies that the output size is always the input size minus a 2-pixel-on-each-side border - the output is centred with respect to the input.

# 2x 4x4 Deconvolution Layer

The deconvolution layer takes the existing 256ch image and expands it outwards into the final 3ch scaled image.

It is very similar to a convolution layer in internal layout, but the positions within the kernel describe positions in the output points, rather than the input points.

This is confusing enough that rather than describing any form of theory at all, I'll describe the practice:

Each deconvolution kernel instance maps to each input point, and to a set of output points.

The output points are initialized to bias values, and then have all relevant deconvolution kernel instances applied to them.

There is a 2x2 grid onto which 4x4 deconvolution kernel instances are placed, i.e. the deconvolutions overlap with each other.

As such, the output layer size is the input size multiplied by 2, with 2 pixels added on the right and bottom edges.

In practice, however, there is a 3-pixel-on-each-side border considered "padding", removed as a post-processing step.

Adherence to this could probably be kept out of the core implementation.

But on the flip-side, implementing at least a 2-pixel-on-each-side border "natively" allows avoiding certain bounds checks, as all pixels are then covered by exactly 4 deconvolution kernel instances in known relative positions.

# Raw Information

`scale2.0x_model.param` (and other upconv7 models)

Keep in mind some information in this file is inferred from the previous layer.

https://github.com/nihui/waifu2x-ncnn-vulkan/blob/master/models/models-upconv_7_photo/scale2.0x_model.param

Output of awful patch to NCNN that reports ModelBin accesses:

```
ModelBinFromDataReader()
ModelBin load 432 0
 0 : 432 of 4
ModelBin load 16 1
 1 : 16 of 4
ModelBin load 4608 0
 2 : 4608 of 4
ModelBin load 32 1
 3 : 32 of 4
ModelBin load 18432 0
 4 : 18432 of 4
ModelBin load 64 1
 5 : 64 of 4
ModelBin load 73728 0
 6 : 73728 of 4
ModelBin load 128 1
 7 : 128 of 4
ModelBin load 147456 0
 8 : 147456 of 4
ModelBin load 128 1
 9 : 128 of 4
ModelBin load 294912 0
 10 : 294912 of 4
ModelBin load 256 1
 11 : 256 of 4
ModelBin load 12288 0
 12 : 12288 of 4
ModelBin load 3 1
 13 : 3 of 4
~ModelBinFromDataReader()
```

# Addendum: vgg7

The vgg7 network is solely made up of 7 RELU Convolution layers.

The expansion is handled outside of the network, a nearest-neighbour 2x.

Detailed breakdown follows:

```
Input                     : Theoretically 156x156x3ch, in practice any not-too-small 3ch
3x3 RELU Convolution Layer: 3ch   -> 32ch
3x3 RELU Convolution Layer: 32ch  -> 32ch
3x3 RELU Convolution Layer: 32ch  -> 64ch
3x3 RELU Convolution Layer: 64ch  -> 64ch
3x3 RELU Convolution Layer: 64ch  -> 128ch
3x3 RELU Convolution Layer: 128ch -> 128ch
3x3 RELU Convolution Layer: 128ch -> 3ch
```

The RELU on the last layer is optional, as obviously it has no effect after clamping.

If you *really* need to cut corners in code size, the vgg7 architecture... runs, at least.

However, it is actually slower than upconv7.

